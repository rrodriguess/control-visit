package br.org.cap.util;

/**
 * Created by user on 04/10/16.
 */
public class RG implements Documento{

    private String numero;

    private Long idPortador;

    @Override
    public String getNumero() {
        return this.numero;
    }

    @Override
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public Long getIdPortador() {
        return this.idPortador;
    }

    @Override
    public void setIdPortador(Long idPortador) {
        this.idPortador = idPortador;
    }
}
