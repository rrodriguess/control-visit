package br.org.cap.util;

/**
 * Created by user on 04/10/16.
 */
public interface Documento{

    String getNumero();

    void setNumero(String numero);

    Long getIdPortador();

    void setIdPortador(Long numero);

}
