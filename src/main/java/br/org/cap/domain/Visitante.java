package br.org.cap.domain;

import br.org.cap.util.Documento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Visitante {

    @Id
    @GeneratedValue
    private Long id;

    private String nome;

    private String documento;

    private String empresa;

    private Long cartao;

    private String portaria;

    private String semana;
    
    private String inicio;

    private String termino;

    private String hrInicio;

    private String hrTermino;

    private String autorizacao;

    private String acesso;

    private String motivo;

    private String placa;

    private String observacao;

    private Boolean garagem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Long getCartao() {
        return cartao;
    }

    public void setCartao(Long cartao) {
        this.cartao = cartao;
    }

    public String getPortaria() {
        return portaria;
    }

    public void setPortaria(String portaria) {
        this.portaria = portaria;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getHrInicio() {
        return hrInicio;
    }

    public void setHrInicio(String hrInicio) {
        this.hrInicio = hrInicio;
    }

    public String getHrTermino() {
        return hrTermino;
    }

    public void setHrTermino(String hrTermino) {
        this.hrTermino = hrTermino;
    }

    public String getTermino() {
        return termino;
    }

    public void setTermino(String termino) {
        this.termino = termino;
    }

    public String getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(String autorizacao) {
        this.autorizacao = autorizacao;
    }

    public String getAcesso() {
        return acesso;
    }

    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getGaragem() {
        return garagem;
    }

    public void setGaragem(Boolean garagem) {
        this.garagem = garagem;
    }

    @Override
    public String toString() {
        return "Visitante{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", documento='" + documento + '\'' +
                ", empresa='" + empresa + '\'' +
                ", cartao=" + cartao +
                ", portaria='" + portaria + '\'' +
                ", semana='" + semana + '\'' +
                ", inicio='" + inicio + '\'' +
                ", termino='" + termino + '\'' +
                ", hrInicio='" + hrInicio + '\'' +
                ", hrTermino='" + hrTermino + '\'' +
                ", autorizacao='" + autorizacao + '\'' +
                ", acesso='" + acesso + '\'' +
                ", motivo='" + motivo + '\'' +
                ", placa='" + placa + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Visitante)) return false;

        Visitante visitante = (Visitante) o;

        return getDocumento().equals(visitante.getDocumento());

    }

    @Override
    public int hashCode() {
        return getDocumento().hashCode();
    }
}
