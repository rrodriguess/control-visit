package br.org.cap.domain.repository;

import br.org.cap.domain.Visitante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitanteDao extends JpaRepository<Visitante, Long>{
}
