package br.org.cap.service.restController;

import br.org.cap.domain.Visitante;
import br.org.cap.service.VisitanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VisitanteRestController {

    @Autowired
    VisitanteService service;

    //************************************* END POINTS **************************************************************
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/visitantes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Visitante> cadastrarVisitante(@RequestBody Visitante visitante) {
        Visitante clienteCadastrado = service.save(visitante);

        return new ResponseEntity<Visitante>(clienteCadastrado, HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/visitantes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Visitante>> buscarTodosClientes() {
        List<Visitante> visitantesEncontrados = service.findAllVisitantes();

        return new ResponseEntity<>(visitantesEncontrados, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/visitantes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Visitante> buscarClientePorId(@PathVariable Long id) {
        Visitante visitante = service.findById(id);

        return new ResponseEntity<>(visitante, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/visitantes/{id}")
    public ResponseEntity<Visitante> removerCliente(@PathVariable Long id) {
        Visitante visitanteEncontrado = service.findById(id);

        if (visitanteEncontrado == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);

        service.remove(visitanteEncontrado);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/visitantes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Visitante> alterarCliente(@RequestBody Visitante visitante) {
        Visitante visitanteAlterado = service.edit(visitante);

        return new ResponseEntity<Visitante>(visitanteAlterado, HttpStatus.OK);
    }

}
