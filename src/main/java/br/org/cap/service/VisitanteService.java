package br.org.cap.service;

import br.org.cap.domain.Visitante;
import br.org.cap.domain.repository.VisitanteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitanteService {

    @Autowired
    VisitanteDao visitanteDao;

    public Visitante save(Visitante visitante) {
        return visitanteDao.save(visitante);
    }

    public List<Visitante> findAllVisitantes() {
        return visitanteDao.findAll();
    }

    public Visitante findById(Long id) {
        return visitanteDao.findOne(id);
    }

    public void remove(Visitante visitante) {
        visitanteDao.delete(visitante);
    }

    public Visitante edit(Visitante visitante) {
        Visitante visitanteCopy = valid(visitante);

        return visitanteDao.save(visitanteCopy);
    }

    private Visitante valid(Visitante visitante) {
        Visitante visitanteBD = visitanteDao.findOne(visitante.getId());

        if (visitante.getAutorizacao() != null)
            visitanteBD.setAutorizacao(visitante.getAutorizacao());
        if (visitante.getCartao() != null)
            visitanteBD.setCartao(visitante.getCartao());
        if (visitante.getDocumento() != null)
            visitanteBD.setDocumento(visitante.getDocumento());
        if (visitante.getEmpresa() != null)
            visitanteBD.setEmpresa(visitante.getEmpresa());
        if (visitante.getInicio() != null)
            visitanteBD.setInicio(visitante.getInicio());
        if (visitante.getTermino() != null)
            visitanteBD.setTermino(visitante.getTermino());
        if (visitanteBD.getPortaria() != null)
            visitanteBD.setPortaria(visitante.getPortaria());
        if (visitante.getNome() != null)
            visitanteBD.setNome(visitante.getNome());
        if (visitante.getSemana() != null)
            visitanteBD.setSemana(visitante.getSemana());
        if (visitante.getGaragem() != null)
            visitanteBD.setGaragem(visitante.getGaragem());
        if (visitante.getPlaca() != null)
            visitanteBD.setPlaca(visitante.getPlaca());
        if (visitante.getObservacao() != null)
            visitanteBD.setObservacao(visitante.getObservacao());
        if (visitante.getAcesso() != null)
            visitanteBD.setAcesso(visitante.getAcesso());
        if (visitante.getMotivo() != null)
            visitanteBD.setMotivo(visitante.getMotivo());

        return visitanteBD;
    }


}
