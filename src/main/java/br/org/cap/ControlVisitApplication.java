package br.org.cap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlVisitApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlVisitApplication.class, args);
	}
}
